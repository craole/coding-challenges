# Coding Challenges

## Hello World

Basic print to stdout

## Guessing Game

## BigInteger

---

A class to perform math operations on numbers with a large amount of digits using only primative structures.

1. Define the class and properties (struct or enum)
2. Implement constructor class to extract and store properties
3. Implement utility methods `(compare_to, to_string)`
4. Implement arithmetic methods `(+,-,*,/,%)`

### Constructor

> _new_ - implement a way to store the properties of the class.

- Number `(String)`
- Sign `(Negative vs Positive)`
- Size `(Absolute)`

### Utility

> _compare_to_ - take another big integer as an argument and return an integer indicating whether the first big integer is greater than, equal to, or less than the second big integer

>_to_string_ - return the big integer value as a string, including the sign and size.

### Arithmetic

| Operation     | Sign | Result     | Formula                            |
| ------------- | ---- | ---------- | ---------------------------------- |
| Addition      | +    | sum        | `addend + addend`                  |
| Subtraction   | -    | difference | `minuend + subtrahend`             |
| Multiplaction | \*   | product    | `multiplicand * multiplier`        |
| Division      | /    | dividend   | `(divisor × quotient) + remainder` |

> Addition - Calculate the sum of big integers
- Accept another big integer
- Convert numbers to an array of BigInt strings
- Find the length of each BigInt string
- add each intiger by character position
- substitute zero for strings shorter strings
- Add digits by character location
  - Determine the longest string
  - Find the position of the last character
  - If all numbers are the same length

> Subtraction - Calculate the difference of big integers

- If the first number is smaller the answer should be negative
- Check for the large number
