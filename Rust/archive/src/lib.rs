#![allow(dead_code, unused_variables, unused_imports)]

mod rust_book;
mod easy_rust;
mod utils;
mod random;
mod rust_cookbook;

use crate::random::big_integer;

fn main() {
    // rust_book::hello_world::main();
    // rust_book::guessing_game::main();

    //@ BigInteger
    big_integer::new("19302324");
    big_integer::add();
    big_integer::subtract();
    big_integer::multiply();
    big_integer::divide();

    // easy_rust::structs::main();
    // rust_book::coin::value_in_cents(Nickel);
    // rust_cookbook::csv::main();
}
