//> Unit
struct FileDirectory;

//> Tuple
#[derive(Debug)]
struct Color(u8, u8, u8);

//> Named
#[derive(Debug)]
struct SizeAndColor {
    size: u32,
    color: Color,
}

pub fn main() {
    let my_directory = FileDirectory;
    let some_color = Color(50, 80, 13);
    let size_and_color = SizeAndColor {
        size: 150,
        color: some_color,
    };

    println!("The first color is {:?}", size_and_color)
}
