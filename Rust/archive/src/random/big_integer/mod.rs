pub mod big_integer;
pub mod addition;
pub mod subtraction;
pub mod multiplication;
pub mod division;

use crate::utils::is_string::numeric;

pub fn new(str: &str) {
    println!("{}", numeric(&str));
}

pub fn add() {
    // let number = String::new();
    println!("BigInt.add")
}

pub fn subtract() {
    // let number = String::new();
    println!("BigInt.subtract")
}

pub fn multiply() {
    // let number = String::new();
    println!("BigInt.multiply")
}

pub fn divide() {
    // let number = String::new();
    println!("BigInt.divide")
}
