pub fn is_valid_number<T>(input: T) -> bool where T: ToString {
    // Converts input to a string
    let num_str = input.to_string();

    // Keep track of whether we've seen a decimal point or a sign
    let mut decimal_seen = false;
    let mut sign_seen = false;

    // Iterate over the characters in the string
    for ch in num_str.chars() {
        if ch == '-' || ch == '+' {
            // If we've already seen a sign or a decimal point,
            // the input is not a valid number
            if sign_seen || decimal_seen {
                return false;
            }

            // Otherwise, mark that we've seen a sign
            sign_seen = true;
        } else if ch == '.' {
            // If we've already seen a decimal point,
            // the input is not a valid number
            if decimal_seen {
                return false;
            }

            // Otherwise, mark that we've seen a decimal point
            decimal_seen = true;
        } else if ch == 'e' || ch == 'E' {
            // If we see an 'e' or 'E', we don't need to check
            // for any more decimal points or signs, because
            // scientific notation allows for them
            break;
        } else if !ch.is_numeric() {
            // If we see any other non-numeric character,
            // the input is not a valid number
            return false;
        }
    }

    // If we get through the entire string without returning false,
    // then the input is a valid number
    true
}
