// pub fn add() {
//     // let number = String::new();
//     println!("BigInt.Add")
// }

// add.rs

// use super::BigInt;

impl Add for BigInteger {
    type Output = BigInteger;

    fn add(self, other: BigInteger) -> BigInteger {
        // Initialize the `result` variable to an empty `BigInteger`.
        let mut result = BigInteger {
            digits: String::new(),
            is_positive: true,
        };

        // Get the longer and shorter `BigInteger`s.
        let (longer, shorter) = if self.digits.len() >= other.digits.len() {
            (self, other)
        } else {
            (other, self)
        };

        // Set the sign of the result based on the sign of the longer `BigInteger`.
        result.is_positive = longer.is_positive;

        // Keep track of the carry while adding the digits.
        let mut carry = 0;
        for i in 0..longer.digits.len() {
            let d1 = longer.digits[i];
            let d2 = if i < shorter.digits.len() {
                shorter.digits[i]
            } else {
