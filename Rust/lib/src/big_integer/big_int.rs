trait BigInt {
    // Add two large integers, represented as strings
    fn add(self, other: &str) -> String;

    // Subtract two large integers, represented as strings
    fn subtract(self, other: &str) -> String;

    // Multiply two large integers, represented as strings
    fn multiply(self, other: &str) -> String;

    // Divide two large integers, represented as strings
    fn divide(self, other: &str) -> String;
}

// Implement the BigInt trait for the `str` primitive type
impl BigInt for str {
    fn add(self, other: &str) -> String {
        // Convert the input strings to Vec<u8> for easier manipulation
        let mut num1: Vec<u8> = self.chars().rev().map(|ch| ch as u8 - 48).collect();
        let mut num2: Vec<u8> = other.chars().rev().map(|ch| ch as u8 - 48).collect();

        // Ensure that num1 is the longer of the two numbers
        if num1.len() < num2.len() {
            std::mem::swap(&mut num1, &mut num2);
        }

        // Perform the addition
        let mut result = Vec::with_capacity(num1.len() + 1);
        let mut carry = 0;
        for i in 0..num1.len() {
            let mut sum = num1[i] + carry;
            if i < num2.len() {
                sum += num2[i];
            }
            result.push(sum % 10);
            carry = sum / 10;
        }
        if carry > 0 {
            result.push(carry);
        }

        // Convert the result back to a string and return it
        result.into_iter().rev().map(|x| (x + 48) as char).collect()
    }

    fn subtract(self, other: &str) -> String {
        // Convert the input strings to Vec<u8> for easier manipulation
        let mut num1: Vec<u8> = self.chars().rev().map(|ch| ch as u8 - 48).collect();
        let mut num2: Vec<u8> = other.chars().rev().map(|ch| ch as u8 - 48).collect();

        // Ensure that num1 is the larger of the two numbers
        if num1.len() < num2.len() {
            std::mem::swap(&mut num1, &mut num2);
        }

        // Perform the subtraction
        let mut result = Vec::with_capacity(num1.len());
        let mut borrow = 0;
        for i in 0..num1.len() {
            let mut diff = num1[i] - borrow;
            if i < num2.len() {
                diff -= num2[i];
            }
            if diff < 0 {
                diff += 10;
                borrow = 1;
            } else {
                borrow = 0;
            }
            result.push(diff);
        }

        // Remove leading zeros from the result
        while result.len() > 1 && result[result.len() - 1] == 0 {
