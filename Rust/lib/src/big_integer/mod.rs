// pub mod new;
// pub mod addition;
// pub mod subtraction;
// pub mod multiplication;
// pub mod division;

pub enum Sign {
    Positive,
    Negative,
}

pub struct BigInteger {
    // Use a string to store the digits of the number.
    digits: String,

    // Use a `bool` to store the sign of the number (`true` for positive and `false` for negative).
    is_positive: bool,
}

impl BigInteger {
    // Implement a method to convert a string to a `BigInteger`.
    fn from_str(s: &str) -> Result<BigInteger, String> {
        // Initialize the `digits` string and the `is_positive` variable.
        let mut digits = String::new();
        let mut is_positive = true;

        // Check the sign of the number.
        let mut chars = s.chars();
        match chars.next() {
            Some('-') => {
                is_positive = false;
            }
            Some('+') => {}
            Some(c) => {
                if !c.is_digit(10) {
                    return Err(format!("invalid character '{}' at the start of the string", c));
                }
                digits.push(c);
            }
            None => {
                return Err("empty string".to_string());
            }
        }

        // Parse the rest of the digits.
        for c in chars {
            if !c.is_digit(10) {
                return Err(format!("invalid character '{}' in the string", c));
            }
            digits.push(c);
        }

        Ok(BigInteger {
            digits,
            is_positive,
        })
    }
}
