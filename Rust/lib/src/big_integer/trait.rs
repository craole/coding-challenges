trait BigInt {
    // Add two large integers, represented as strings
    fn add(self, other: &str) -> String;

    // Subtract two large integers, represented as strings
    fn subtract(self, other: &str) -> String;

    // Multiply two large integers, represented as strings
    fn multiply(self, other: &str) -> String;
}
