// use lib::*;
use lib::utils::input::*;

fn main() {
    // rust_book::hello_world::main();
    // rust_book::guessing_game::main();

    //@ BigInteger
    // big_integer::new("19302324");
    // big_integer::new("19302324");
    // big_integer::add();
    // big_integer::subtract();
    // big_integer::multiply();
    // big_integer::divide();

    let num = 901.98;

    if is_valid_number(num) {
        println!("{} is a valid number", num)
    } else {
        println!("{} is not a valid number", num)
    }
}
