module Addition

  #? Create an instance within a class
  def %(other_bigint)

    #? Convert the second number to an array
    num_array2 = other_bigint.num_array

    #? Compare the length of each string
    if @num_array.length >= num_array2.length
      addend1 = @num_array
      addend2 = num_array2
    else
      addend1 = num_array2
      addend2 = @num_array
    end

    #? Reverse the numbers to allow calculation
    addend1 = addend1.reverse
    addend2 = addend2.reverse

    #? Initialize local variables
    i = 0
    sum = ""
    carry_over = 0

    #? Generate a loop based on the longest number
    max_chars = (addend1.length - 1)


    for i in 0..(max_chars)

      #?
      sum_val = addend2[i].to_i + addend1[i].to_i + carry_over.to_i
      if sum_val >= 10
        new_val = sum_val % 10
        carry_over = 1
        sum += new_val.to_s
      else
        carry_over = 0
        sum += sum_val.to_s
      end
    end

    sum += "1" if carry_over == 1
    BigInteger.new(sum.reverse)
  end
end
