#| Import
dir = "big_integer"
Dir[File.join(__dir__, dir, "*.rb")].each { |file| require file }

class BigInteger
  #| Declare variables and methods
  include Addition
  include Subtraction
  include Multiplation
  include Division

  #| Ruby sugar to create a 'read' method {reader, writer, accessor}
  attr_reader :num_array

  #| Create an instance if the class
  def initialize(num)
    #| Try to convert input to a string
    str_num = num.to_s

    #| Ensure that all characters are valid numbers
    raise "invalid number" unless str_num.match(/^[0-9]*$/)

    #| Create an instance variable to hold the input
    @num = str_num

    #| Create an array to hold each character of the input
    @num_array = str_num.split("")
  end

  #| Allow the class to be returned using the default print method
  def to_s
    @num
  end

  #| Calculate the length of strings
  def length
    @num.length
  end

  #| Addition
  # def self.add(num1, num2)
  #   num1 = BigInteger.new(num1)
  #   num2 = BigInteger.new(num2)

  #   puts num1 + num2
  # end

  def self.add(*array)
    # sum = array.length
    # sum = BigInteger.new(array[0]) + BigInteger.new(array[1])
    numbers = self.new(*array)
    #  @num_array.each_with_index do |val, index|
    # if val.to_i > num_array2[index].to_i
    #   minuend = @num_array
    #   subtrahend = num_array2
    #   return

    # array.each { |a| puts BigInteger.new(a) + BigInteger.new(a) }
    # puts "The numbers are {array}"
  end
end
