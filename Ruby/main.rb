#| Import
dir = "lib"
Dir[File.join(__dir__, dir, "*.rb")].each { |file| require file }

#| Variables
# num1 = BigInteger.new("10s")
# num2 = BigInteger.new("70s")
# num3 = BigInteger.new("20")

#| Run
# puts num1 + num2 + num3
puts BigInteger.new(21)
# puts num1 - num2
# puts num1 * num2 * num3
# puts num1.add(num1)
