class BigInt

  #| Ruby sugar to create a 'read' method {reader, writer, accessor}
  attr_reader :num_array

  #| Allow the class to be returned using the default print method
  def to_s
    @num
  end

  #| Calculate the length of strings
  def length
    @num.length
  end

  #| Create an new instance if the class
  def initialize(num)
    #| Try to convert input to a string
    str_num = num.to_s

    #| Ensure that all characters are valid numbers
    raise "invalid number" unless str_num.match(/^[0-9]*$/)

    #| Create an instance variable to hold the input
    @num = str_num

    #| Create an array to hold each character of the input
    @num_array = str_num.split("")
  end

  def self.add2(greater_num_arr, smaller_num_arr)
    greater_num_arr = greater_num_arr.reverse
    smaller_num_arr = smaller_num_arr.reverse

    i = 0
    result = ""
    carry_over = 0
    for i in 0..(greater_num_arr.length - 1)
      sum_val = smaller_num_arr[i].to_i + greater_num_arr[i].to_i + carry_over.to_i
      if sum_val >= 10
        new_val = sum_val % 10
        carry_over = 1
        result += new_val.to_s
      else
        carry_over = 0
        result += sum_val.to_s
      end
    end

    result += "1" if carry_over == 1
    BigInteger.new(result.reverse)
  end

  def self.add(num1, num2)
    puts num1.reverse()
    puts num2
  end
  #| Addition Method
  def +(*array)
    array.each do |n|
      num = BigInt.new(n)
      text = "Current number is: #{n}"
      puts num
    end
  end
end
