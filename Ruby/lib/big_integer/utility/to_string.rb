#| Allow the class to be returned using the default print method
class BigInteger

  # def self.to_string
  #   @num
  # end

  def self.to_string(object)
    object.to_s
  end
end
