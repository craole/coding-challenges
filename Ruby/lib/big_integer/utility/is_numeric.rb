
def raise_error_if_not_numeric(string)
  integer = string.to_i
  raise TypeError, "The string is not numeric" unless integer
end
