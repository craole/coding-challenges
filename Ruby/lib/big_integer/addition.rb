#@ Addition of BigIntegers
#@ Formula: addennd + addend = sum
#@ Example: 1562831 + 129012 = 1691843
module Addition

  #? Create an instance within a class
  def +(other_addend)

    #? Reverse the addend string arrays to allow calculation
    addend_1 = @num_array.reverse
    addend_2 = other_addend.num_array.reverse

    #? Identify the longest addend string
    if addend_1.length >= addend_2.length
      addend_longest = addend_1.length
    else
      addend_longest = addend_2.length
    end

    #? Initialize loop variables
    i = 0
    sum = ""
    carry = 0

    #? Calculate via regrouping per the digit index
    for i in 0..(addend_longest - 1)

      #? Equation: SUM
      sum = addend_1[i].to_i +
            addend_2[i].to_i +
            carry.to_i

      #? Calculate carry forward and remainder
      if sum > 10
        sum = sum % 10
        carry = 1
      end

      #? Update the sum
      # sum += sum.to_s
    end

    #? Add one to current place value if carry exists
    sum += "1" if carry == 1

    #? Return the total as a BigInteger
    BigInteger.new(sum.reverse)
  end
end
