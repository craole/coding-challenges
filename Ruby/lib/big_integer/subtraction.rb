#@ Subtract BigIntegers
#@ Formula: minuend - subtrahend = difference
#@ Example: 10 - 7 = 3
module Subtraction
  def -(other_bigint)

    num_array2 = other_bigint.num_array

    if @num_array.length > num_array2.length
      minuend = @num_array
      subtrahend = num_array2
    elsif num_array2.length > @num_array.length
      minuend = num_array2
      subtrahend = @num_array
    else
      # same length
      # iterate over the nums and compare each char num
      minuend = @num_array
      subtrahend = num_array2

      @num_array.each_with_index do |val, index|
        if val.to_i > num_array2[index].to_i
          minuend = @num_array
          subtrahend = num_array2
          return
        else num_array2[index].to_i > val.to_i
          minuend = num_array2
          subtrahend = @num_array
          return
        end
      end
    end

    minuend = minuend.reverse
    subtrahend = subtrahend.reverse
  end
end
