class BigInteger

  #| Import files
  dir = "big_integer"
  Dir[File.join(__dir__, dir, "**/*.rb")].each { |file| require file }

  #| Load modules
  # include
  include Addition
  include Subtraction
  include Multiplation
  include Division

  # #| Ruby sugar to create a 'read' method {reader, writer, accessor}
  attr_reader :num_split

  # #| Create an instance if the class
  def initialize(value)

    #| Ensure that all characters are valid numbers
    raise "Invalid: Only numbers are allowed" unless
      value.to_s.match(/^[0-9]*$/)

    #| Create an instance variable to hold the input
    @num = value.to_s

    #| Create an array to hold each character of the number
    @num_split = @num.split("")

    puts @num
  end

  def to_string
    @num
  end
end
